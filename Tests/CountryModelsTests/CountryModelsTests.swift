import XCTest
@testable import CountryModels
@testable import Canada

final class CountryModelsTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CountryModels().text, "Hello, World!")
    }
    
    testCanada() throws {
        let result = "Stupid".hello
        XCTAssertEqual(result, "Hello Stupid!")
    }
}
