//
//  DataStructsAndModifiers.swift
//  Covid19
//
//  Created by Frederick C. Lee on 1/1/21.
//

import Foundation
import MapKit

typealias RawDataDict = [Date: Double]

let tapToShowButtonTitle = "Tap to Display Countries"

struct Coordinates: Codable, Hashable {
    let latitude, longitude: String
}

struct RevisedCoordinates {
    let latitude, longitude: CLLocationDegrees
}

struct CountryMap: Hashable {
    let coordinates: Coordinates
    let title: String?
}

struct Stats: Codable, Hashable {
    let confirmed, deaths, recovered: Int?
}

struct CountryIDItem: Hashable {
    let id: String
    let name: String
}

struct ResponseStatus {
    var errorFound: Bool = false
    var message: String = ""
}
