
//
//  Extension.swift
//  Covid19
//
//  Created by Frederick C. Lee on 11/26/20.
//

import Foundation

public struct Resource {
    let name = "Ric"
}
extension String {
    public var hello: String {
        return "Hello \(self)!"
    }
    
    public var ageRange: String {
        if self == "0 - 9" {
            return "Ages: 0 thru 9 years"
        } else if self == "10 - 19" {
            return "Ages: 10 thru 19 years"
        } else if self == "20 - 29" {
            return "Ages: 20 thru 29 years"
        } else if self == "30 - 39" {
            return "Ages: 30 thru 39 years"
        } else if self == "40 - 49" {
            return "Ages: 40 thru 49 years"
        } else if self == "50 - 59" {
            return "Ages: 50 thru 59 years"
        } else if self == "60 - 69" {
            return "Ages: 60 thru 69 years"
        } else if self == "70 - 79" {
            return "Ages: 70 thru 79 years"
        } else if self == "80 - 89" {
            return "Ages: 80 thru 89 years"
        } else {
            return "Ages: Over 90 years"
        }
    }

    public var double: Double {
        Double(self) ?? 0
    }

    public func matches(_ regex: String) -> Bool {
        range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }

    public func remove(substring: String) -> String {
        replacingOccurrences(of: substring, with: "")
    }

    public func replace(substring: String, with _: String) -> String {
        replacingOccurrences(of: substring, with: "")
    }

    public var camel: String {
        let array = split(separator: " ")
        var str: String = ""
        array.forEach { item in
            let camelWord = (item.first?.uppercased())! + item.dropFirst()
            str.append(camelWord)
            str.append(" ")
        }
        return String(str.dropLast())
    }

    public var cleanse: String {
        let substring = "&rsquo"

        if contains(substring) {
            return replacingOccurrences(of: substring, with: "")
        }

        let substring2 = ",&nbsp"
        if contains(substring2) {
            return replacingOccurrences(of: substring2, with: "")
        }

        return self
    }

    // =========================================================
    // Dates

    public var inputFormat: String? {
        let pattern1 = #"^\d\d\D\d\d\D\d\d"# // mm- or dd-
        let pattern2 = #"^\d\d\d\d\D\d\d\D\d\d"# // yyyy-
        let pattern3 = #"\d\d\\D\d\d\D\d\d\d\d"# // -yyyy
        let pattern4 = #"\d\d\d\d\d\d\d\d"# // 20201124

        // TODO: make RegExp parsing more intelligent
        // Input Format:
        if matches(pattern1) {
            return "MM/dd/yy" // 11/24/20 --> 24 November 2020
        } else if matches(pattern2) {
            return "yyyy-mm-dd"
        } else if matches(pattern3) {
            return "MM/dd/yyyy"
        } else if matches(pattern4) {
            return "yyyyMMdd"
        }

        return nil
    }

    // ---------------------------------------------
    // "2021-01-10 05:22:12" --> January 1, 2021

    public var isoToStandard: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            print("*** String.isoToStandard conversion Error*** for Date: \(self)")
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "MMMM d, yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    public var MMddyyToDate: Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MM/dd/yy"

        let date = dateFormat.date(from: self)
        return date
    }

    // ---------------------------------------------
    // 11/24/20 --> 20201124

    public var dateKey: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MM/dd/yy"

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            print("*** String.dateKey conversion Error*** for Date: \(self)")
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "yyyyMMdd"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "31-01-2020" --> 20201124

    public var dateKey2: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            print("*** String.dateKey conversion Error*** for Date: \(self)")
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "yyyyMMdd"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "31 January 2020":
    public var dayFullMonthYear: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = inputFormat ?? ""

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "d MMMM yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }
    
    // ---------------------------------------------
    //  "2021-03-05 05:26:29" to "March 5, 2021":
    
    public var fullMonthDayYearDelux: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd hh:mm:ss"

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "MMMM d, yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    //  "2020-12-24" to "December 24, 2020":
    public var fullMonthDayYear: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "MMMM d, yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    //  "2020-12-24" to RawDate:
    public var rawDate: Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"

        if let date = dateFormat.date(from: self) {
            return date
        }

        return nil
    }

    // ---------------------------------------------
    // "1/10/21" to RawDate2:
    public var rawDate2: Date? {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MM/dd/yy"

        if let date = dateFormat.date(from: self) {
            return date
        }

        return nil
    }

    // ---------------------------------------------
    // "31-01-2020" --> "2020-01-31":
    public var yyyyMMdd: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = inputFormat ?? ""

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "yyyy-MM-dd"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "31-01-2020" --> "31 January 2020":
    public var ddMMMMyyyy: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = inputFormat ?? ""

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "dd MMMM yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "31-01-2020" --> "January 31, 2020":
    public var ddMMMMyyyy2: String {
        let dateFormat = DateFormatter()
        // Input Format:
        dateFormat.dateFormat = "dd-MM-yyyy"

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "MMMM dd, yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "01-31-2020"" --> "31 January 2020":
    public var MMddyyyy: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = inputFormat ?? ""

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }
        // Output Format:
        dateFormat.dateFormat = "dd MMMM yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "01-31-2020"" --> "January 31, 2020":

    public var MMddyyyy2: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = inputFormat ?? ""

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }

        // Output Format:
        dateFormat.dateFormat = "MMMM d, yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ---------------------------------------------
    // "2020-01-31" --> "31-01-2020"
    public var ddMMyyyy: String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = inputFormat ?? ""

        let date = dateFormat.date(from: self)
        guard let convertedDate = date else {
            return ""
        }

        // Output Format:
        dateFormat.dateFormat = "dd-MM-yyyy"
        let formattedDateString = dateFormat.string(from: convertedDate)
        return formattedDateString
    }

    // ========================================================================================

    public var removeHTMLTag: String {
        var revisedString = replacingOccurrences(of: "&rsquo", with: "'")
        revisedString = revisedString.replacingOccurrences(of: "&nbsp;", with: " ")
        revisedString = revisedString.replacingOccurrences(of: "\n", with: "")
        return revisedString
    }

    public var reformat: String {
        var revisedString = replacingOccurrences(of: "&nbsp;", with: " ")
        revisedString = revisedString.replacingOccurrences(of: "Trials: ", with: "\n\nTrials: ")
        revisedString = revisedString.replacingOccurrences(of: "Trial:", with: "\n\nTrial:")
        revisedString = revisedString.replacingOccurrences(of: ". Merck ", with: ".\n\nMerck ")
        revisedString = revisedString.replacingOccurrences(of: "Status:", with: "\n\nStatus:")
        revisedString = revisedString.replacingOccurrences(of: "Results ", with: "\n\nResults ")
        revisedString = revisedString.replacingOccurrences(of: "Further ", with: "\n\nFurther ")
        revisedString = revisedString.replacingOccurrences(of: "Actions:", with: "\n\nActions:")
        revisedString = revisedString.replacingOccurrences(of: "Topline ", with: "\n\nTopline ")
        revisedString = revisedString.replacingOccurrences(of: "However,", with: "\n\nHowever,")
        revisedString = revisedString.replacingOccurrences(of: "For ", with: "\n\nFor ")
        revisedString = revisedString.replacingOccurrences(of: ". The ", with: ".\n\nThe ")
        revisedString = revisedString.replacingOccurrences(of: "Early ", with: "\n\nEarly ")
        revisedString = revisedString.replacingOccurrences(of: "Regulatory actions:", with: "\n\nRegulatory actions:")
        revisedString = revisedString.replacingOccurrences(of: " The approval:", with: "\n\nThe approval")
        revisedString = revisedString.replacingOccurrences(of: "In ", with: "\n\nIn ")
        revisedString = revisedString.replacingOccurrences(of: "International:", with: "\n\nInternational:")
        revisedString = revisedString.replacingOccurrences(of: " On ", with: "\n\nOn ")
        revisedString = revisedString.replacingOccurrences(of: "(see below).", with: "(see below).\n\n")
        revisedString = revisedString.replacingOccurrences(of: " - ", with: "\n\n- ")
        revisedString = revisedString.replacingOccurrences(of: " Outcomes:", with: "\n\nOutcomes:")
        revisedString = revisedString.replacingOccurrences(of: " Outcome:", with: "\n\nOutcome:")
        revisedString = revisedString.replacingOccurrences(of: " Evidence for benefit:", with: "\n\nEvidence for benefit:")
        return revisedString
    }
}

// =======================================================================================================

extension String {
    private func filterCharacters(unicodeScalarsFilter closure: (UnicodeScalar) -> Bool) -> String {
        String(String.UnicodeScalarView(unicodeScalars.filter { closure($0) }))
    }

    private func filterCharacters(definedIn charSets: [CharacterSet], unicodeScalarsFilter: (CharacterSet, UnicodeScalar) -> Bool) -> String {
        if charSets.isEmpty { return self }
        let charSet = charSets.reduce(CharacterSet()) { $0.union($1) }
        return filterCharacters { unicodeScalarsFilter(charSet, $0) }
    }

    func removeCharacters(charSets: [CharacterSet]) -> String { filterCharacters(definedIn: charSets) { !$0.contains($1) } }
    func removeCharacters(charSet: CharacterSet) -> String { removeCharacters(charSets: [charSet]) }

    func onlyCharacters(charSets: [CharacterSet]) -> String { filterCharacters(definedIn: charSets) { $0.contains($1) } }
    func onlyCharacters(charSet: CharacterSet) -> String { onlyCharacters(charSets: [charSet]) }
}

extension String {
    var onlyDigits: String { onlyCharacters(charSets: [.decimalDigits]) }
    var onlyLetters: String { onlyCharacters(charSets: [.letters]) }
}
