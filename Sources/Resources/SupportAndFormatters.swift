//
//  SupportAndFormatters.swift
//  Covid19
//
//  Created by Frederick C. Lee on 12/11/20.
//

import Foundation

func toFormattedString(_ number: Double) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.groupingSeparator = ","
    let formattedString = formatter.string(for: number)
    return formattedString!
}

extension Double {
    var str: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.usesSignificantDigits = false
        formatter.groupingSeparator = ","
        formatter.minimumIntegerDigits = 0
        let formattedString = formatter.string(for: self)
        return formattedString!
    }
}

extension Int {
    var str: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.usesSignificantDigits = false
        formatter.minimumIntegerDigits = 0
        formatter.groupingSeparator = ","
        let formattedString = formatter.string(for: self)
        return formattedString!
    }
}

extension Date {
    var toMMMMdyyyyString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d, yyyy"
        return dateFormatter.string(from: self)
    }

    var toMMMdyyyyString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        return dateFormatter.string(from: self)
    }
}
