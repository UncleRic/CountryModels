//
//  AustriaModel.swift
//
//
//  Created by Frederick Lee on 10/14/21.
//

import Combine
import Foundation

public final class AustriaModel: ObservableObject {
    static let shared = AustriaModel()
//    @Published var austriaData: RevisedAustriaDataList?

    public init() {}

    var revisedAustriaData: RevisedAustriaDataList?

    struct AustriaDataList: Codable {
        let updated: Int
        let provinces: [AustriaProvince]
        let districts: [AustriaDistrict]
        let percentageBySex: PercentageBySex
        let casesByAge, deathsByAge: [String: Int]
    }

    struct AustriaDistrict: Codable {
        let district: String
        let cases, population: Int
    }

    struct PercentageBySex: Codable {
        let cases, deaths: DeathCases
    }

    struct DeathCases: Codable {
        let female, male: Int
    }

    struct AustriaProvince: Codable {
        let province: String
        let cases, recovered, deaths: Int
    }

    // ----------------------------------------------------------
    // Revision:

    public struct RevisedAustriaDataList {
        let provinces: [RevisedAustriaProvince]
        let districts: [RevisedAustriaDistrict]
        let percentageBySex: PercentageBySex
        let casesByAge, deathsByAge: [String: Int]
    }

    struct RevisedAustriaProvince: Identifiable {
        let id: UUID
        let province: String
        let cases, recovered, deaths: String
    }

    struct RevisedAustriaDistrict: Identifiable {
        let id: UUID
        let district: String
        let cases, population: String
    }

    enum MyError: Error {
        case failed
        case inValidURL
        case httpError(Int)
        case missingData
    }

    // ==============================================================================================

    public func getData() -> RevisedAustriaDataList? {
        
        if #available(iOS 15.0, *) {
            Task {
                do {
                    try await fetchDataWithAsyncURLSession()
                } catch {
                    print("Disease Error \(error)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        return revisedAustriaData
    }

    // ----------------------------------------------------------------------------------------------
    @available(iOS 15.0.0, *)

    private func fetchDataWithAsyncURLSession() async throws  {
        guard let url = URL(string: "https://disease.sh/v3/covid-19/gov/Austria") else {
            throw MyError.inValidURL
        }

        // Use the async variant of URLSession to fetch data
        let (data, _) = try await URLSession.shared.data(from: url)

        // Parse the JSON data
        let austriaData = try JSONDecoder().decode(AustriaDataList.self, from: data)
        self.revisedAustriaData = add_UUID(origData: austriaData)
    }

    // ----------------------------------------------------------------------------------------------
    private func fetchData() {
        let str = "https://disease.sh/v3/covid-19/gov/Austria"
        let url = URL(string: str)!
        var cancellables: Set<AnyCancellable> = []
        let remoteDataPublisher = URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .receive(on: DispatchQueue.main)
            .decode(type: AustriaDataList.self, decoder: JSONDecoder())

        remoteDataPublisher
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    print("{AustriaModel} Publisher Finished")
                case let .failure(anError):
                    Swift.print("\nAustria - Received error: #function", anError)
                }
            }, receiveValue: { someData in
                self.revisedAustriaData = self.add_UUID(origData: someData)
            }).store(in: &cancellables)

    }

    // ----------------------------------------------------------------------------------------------
    private func add_UUID(origData: AustriaDataList) -> RevisedAustriaDataList? {
        var districts: [RevisedAustriaDistrict] = []
        var provinces: [RevisedAustriaProvince] = []

        // Districts:
        for item in origData.districts {
            let element = RevisedAustriaDistrict(
                id: UUID(),
                district: item.district,
                cases: item.cases.str,
                population: item.population.str
            )
            districts.append(element)
        }

        // Provinces:
        for item in origData.provinces {
            let element = RevisedAustriaProvince(
                id: UUID(),
                province: item.province,
                cases: item.cases.str,
                recovered: item.recovered.str,
                deaths: item.deaths.str
            )
            provinces.append(element)
        }

        revisedAustriaData = RevisedAustriaDataList(
            provinces: provinces,
            districts: districts,
            percentageBySex: origData.percentageBySex,
            casesByAge: origData.casesByAge,
            deathsByAge: origData.deathsByAge
        )

        return revisedAustriaData
    }
}

// ----------------------------------------------------------------------------------------------
// TODO: Put extension into global resource folder.

extension Int {
    var str: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.usesSignificantDigits = false
        formatter.minimumIntegerDigits = 0
        formatter.groupingSeparator = ","
        let formattedString = formatter.string(for: self)
        return formattedString!
    }
}
